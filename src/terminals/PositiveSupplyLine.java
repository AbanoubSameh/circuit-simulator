/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class PositiveSupplyLine extends Node {

	private final int voltage;

	public PositiveSupplyLine(int voltage) {
		this.voltage = voltage;
	}

	@Override
	void setVoltage(int voltage, Component component) {

		if (voltage != this.voltage && voltage != -1) {
			System.out.println("Short Circuit: " + this.toString() + component.toString());
		}

	}

	@Override
	int getVoltage(Component component) {
		System.out.println("returned 220v to " + component.toString());
		return voltage;
	}

}
