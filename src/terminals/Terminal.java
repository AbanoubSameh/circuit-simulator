/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Terminal {

	private Terminal connection;
	private Component component;
	private int voltage;
	private boolean source;

	public Terminal(Component component) {
		this.component = component;
	}

	public void setVoltage(int voltage) {
		this.voltage = voltage;
	}

	public int getVoltage() {
		return voltage;
	}

	public void setConnection(Terminal connection) {
		this.connection = connection;
	}

	public Terminal getConnection() {
		return connection;
	}

	public void setSource(boolean source) {
		this.source = source;
	}

	public boolean isSource() {
		return source;
	}

	public Component getComponent() {
		return component;
	}

}
