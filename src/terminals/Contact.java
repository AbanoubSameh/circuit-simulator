/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Contact extends Component {

	private Relay relay;
	private boolean closed;

	Contact(Relay relay, boolean closed) {
		this.relay = relay;
		this.closed = closed;
	}

	@Override
	void setVoltage(int voltage, Component component) {

		if (closed) {

			if (component == next && previous != null) {
				previous.setVoltage(voltage, this);
			} else if (component == previous && next != null) {
				next.setVoltage(voltage, this);
			}

		}

	}

	@Override
	int getVoltage(Component component) {

		if (closed) {

			if (component == next && previous != null) {
				if (pass(previous)) {
					return -1;
				}
				return previous.getVoltage(this);
			} else if (component == previous && next != null) {
				if (pass(next)) {
					return -1;
				}
				return next.getVoltage(this);
			} else {
				return -1;
			}

		} else {
			return -1;
		}

	}

	void close() {

		if (closed) {
			return;
		}

		System.out.println(this.toString() + " closed");
		closed = true;

		if (next != null && previous != null) {

			int volt1 = next.getV(this);
			int volt2 = previous.getV(this);

			if (volt1 > volt2) {
				previous.setV(volt1, this);
			} else if (volt2 > volt1) {
				next.setV(volt2, this);
			}

		}

	}

	void open() {

		if (!closed) {
			return;
		}

		System.out.println(this.toString() + " opened");
		closed = false;

		if (next != null && previous != null) {

			next.setV(-1, this);
			previous.setV(-1, this);

		}

	}

	@Override
	public void disconnect() {
		relay.removeContact(this);
	}

}
