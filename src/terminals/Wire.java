/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

import java.util.LinkedList;

public class Wire extends Component {

	private static LinkedList<Wire> wires = new LinkedList<Wire>();

	public Wire() {
		wires.add(this);
	}

	public static Wire getWire(Component first, Component second) {
		Wire toReturn = null;

		for (Wire wire : wires) {
			if ((wire.next == first && wire.previous == second) || (wire.previous == first && wire.next == second)) {
				toReturn = wire;
			}
		}

		return toReturn;
	}

	@Override
	public void disconnect() {
		super.disconnect();
		for (int i = 0; i < wires.size(); i++) {
			if (wires.get(i) == this) {
				wires.remove(i);
				System.out.println("A wire was removed");
				return;
			}
		}
		System.out.println("no wire was removed");
	}

}
