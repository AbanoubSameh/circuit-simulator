/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

import java.util.LinkedList;

public abstract class Component {

	Component next, previous;

	static Component first;
	static LinkedList<Component> passed = new LinkedList<Component>();

	static boolean pass(Component toPass) {

		for (Component component : passed) {
			if (component == toPass) {
				System.out.println("component " + toPass + " exists");
				return true;
			}
		}

		System.out.println("component " + toPass + " does not exist");
		passed.add(toPass);
		return false;
	}

	static void resetPass() {
		passed = new LinkedList<Component>();
	}

	void setV(int voltage, Component component) {
		setVoltage(voltage, component);
	}

	int getV(Component component) {
		resetPass();
		return getVoltage(component);
	}

	void setVoltage(int voltage, Component component) {

		if (component == next && previous != null) {
			previous.setVoltage(voltage, this);
		} else if (component == previous && next != null) {
			next.setVoltage(voltage, this);
		}

	}

	int getVoltage(Component component) {

		if (component == next && previous != null) {
			if (pass(previous)) {
				return -1;
			}
			return previous.getVoltage(this);
		} else if (component == previous && next != null) {
			if (pass(next)) {
				return -1;
			}
			return next.getVoltage(this);
		} else {
			return -1;
		}

	}

	void setNext(Component next) {
		this.next = next;
		this.setV(next.getV(this), next);
	}

	void setPrevious(Component previous) {
		this.previous = previous;
		this.setV(previous.getV(this), previous);
	}

	public static void connect(Component first, Component second) {
		first.setNext(second);
		second.setPrevious(first);
	}

	public void disconnect() {

		if (next != null) {

//			System.out.println(this.toString() + " sends to " + next);
			next.setV(-1, this);
			next.previous = null;
			next = null;
		}

		if (previous != null) {

//			System.out.println(this.toString() + " sends to " + previous);
			previous.setV(-1, this);
			previous.next = null;
			previous = null;
		}

	}

}
