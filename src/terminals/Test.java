/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Test {

	public static void main(String[] args) {

		PositiveSupplyLine line = new PositiveSupplyLine(220);
		NegativeSupplyLine nline = new NegativeSupplyLine();

		ButtonNO on = new ButtonNO();
//		ButtonNO on2 = new ButtonNO();
		ButtonNC off = new ButtonNC();

//		Jog jog = new Jog();

		Relay6Contacts relay = new Relay6Contacts();

		Node node = new Node();
//		Wire wire = new Wire();

		LED led = new LED();
		LED led2 = new LED();

		Component.connect(line, on);
		Component.connect(line, relay.getNextContactNO());
		Component.connect(on, node);
		Component.connect(relay.getNextContactNO(), node);

		Component.connect(node, off);
		Component.connect(off, relay);
		Component.connect(relay, nline);

		Component.connect(line, relay.getNextContactNO());
		Component.connect(relay.getNextContactNO(), led);
		Component.connect(led, nline);

		Component.connect(line, relay.getNextContactNC());
		Component.connect(relay.getNextContactNC(), led2);
		Component.connect(led2, nline);

		System.out.println(relay.energized());
		System.out.println(led.isOn());
		System.out.println(led2.isOn());

		on.pulse();
		System.out.println(relay.energized());
		System.out.println(led.isOn());
		System.out.println(led2.isOn());

		off.pulse();
		System.out.println(relay.energized());

		on.pulse();
		System.out.println(relay.energized());

		System.out.println();
		relay.getNextContactNO().disconnect();

		System.out.println(relay.energized());
		System.out.println(led.isOn());
		System.out.println(led2.isOn());

		off.pulse();
		System.out.println(relay.energized());
		System.out.println(led.isOn());
		System.out.println(led2.isOn());

	}

}
