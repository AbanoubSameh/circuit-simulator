/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Relay4Contacts extends Relay {

	private Contact no2 = null, nc2 = null;

	@Override
	void energize() {

		if (!energized) {
			super.energize();
			if (no2 != null)
				no2.close();
			if (nc2 != null)
				nc2.open();
		}

	}

	@Override
	void denergize() {

		if (energized) {
			super.denergize();
			if (no2 != null)
				no2.open();
			if (nc2 != null)
				nc2.close();
		}

	}

	@Override
	public void disconnect() {
		super.disconnect();
		if (nc2 != null)
			no2.disconnect();
		if (nc2 != null)
			nc2.disconnect();
	}

	@Override
	public Component getNextContactNO() {
		if (no2 == null) {
			no2 = new Contact(this, false);
			return no2;
		} else {
			return super.getNextContactNO();
		}

	}

	@Override
	public Component getNextContactNC() {
		if (nc2 == null) {
			nc2 = new Contact(this, true);
			return nc2;
		} else {
			return super.getNextContactNC();
		}

	}

	public Component no2() {
		if (no2 != null)
			return no2;
		else
			return null;
	}

	public Component nc2() {
		if (nc2 != null)
			return nc2;
		else
			return null;
	}

	@Override
	void removeContact(Contact contact) {
		if (contact == no2) {
			no2 = null;
		} else if (contact == nc2) {
			nc2 = null;
		} else {
			super.removeContact(contact);
		}
	}

}
