/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

import frontend.Simulator;

public class Relay extends Component {

	boolean energized = false;
	private Contact no = null, nc = null;

	@Override
	void setVoltage(int voltage, Component component) {

		if (component == next && previous != null) {

			System.out.println("received voltage: " + voltage);
			System.out.println("other voltage" + previous.getVoltage(this));
			System.out.println(Math.abs(previous.getVoltage(this) - voltage));

			if (Math.abs(previous.getVoltage(this) - voltage) == 220) {
				energize();
			} else {
				denergize();
			}

		} else if (component == previous && next != null) {

			System.out.println("received voltage: " + voltage);
			System.out.println("other voltage: " + next.getVoltage(this));
			System.out.println(Math.abs(next.getVoltage(this) - voltage));

			if (Math.abs(next.getVoltage(this) - voltage) == 220) {
				energize();
			} else {
				denergize();
			}

		}

	}

	@Override
	int getVoltage(Component component) {
		return -1;
	}

	@Override
	public void disconnect() {
		super.disconnect();
		if (no != null)
			no.disconnect();
		if (nc != null)
			nc.disconnect();
	}

	void energize() {

		if (!energized) {
			energized = true;
			if (no != null)
				no.close();
			if (nc != null)
				nc.open();
			System.out.println("Relay energized!!!");
			Simulator.stateChanged(this, energized);
		}

	}

	void denergize() {

		if (energized) {
			energized = false;
			if (no != null)
				no.open();
			if (nc != null)
				nc.close();
			System.out.println("Relay de-energized!!!");
			Simulator.stateChanged(this, energized);
		}

	}

	public Component getNextContactNO() {

		if (no == null) {
			no = new Contact(this, false);
			return no;
		} else {
			System.out.println("no ContactNO remaining for this relay");
			return null;
		}

	}

	public Component getNextContactNC() {

		if (nc == null) {
			nc = new Contact(this, true);
			return nc;
		} else {
			System.out.println("no ContactNC remaining for this relay");
			return null;
		}

	}

	public boolean energized() {
		return energized;
	}

	public Component no() {
		if (no != null)
			return no;
		else
			return null;
	}

	public Component nc() {
		if (nc != null)
			return nc;
		else
			return null;
	}

	void removeContact(Contact contact) {
		if (contact == no) {
			no = null;
		} else if (contact == nc) {
			nc = null;
		} else {
			System.out.println("Can't remove this contact");
		}
	}

}