/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Relay6Contacts extends Relay4Contacts {

	private Contact no3, nc3 = null;

	@Override
	void energize() {

		if (!energized) {
			super.energize();
			if (no3 != null)
				no3.close();
			if (nc3 != null)
				nc3.open();
		}

	}

	@Override
	void denergize() {

		if (energized) {
			super.denergize();
			if (no3 != null)
				no3.open();
			if (nc3 != null)
				nc3.close();
		}

	}

	@Override
	public void disconnect() {
		super.disconnect();
		if (no3 != null)
			no3.disconnect();
		if (nc3 != null)
			nc3.disconnect();
	}

	@Override
	public Component getNextContactNO() {
		if (no3 == null) {
			no3 = new Contact(this, false);
			return no3;
		} else {
			return super.getNextContactNO();
		}

	}

	@Override
	public Component getNextContactNC() {
		if (nc3 == null) {
			nc3 = new Contact(this, true);
			return nc3;
		} else {
			return super.getNextContactNC();
		}

	}

	public Component no3() {
		if (no3 != null)
			return no3;
		else
			return null;
	}

	public Component nc3() {
		if (nc3 != null)
			return nc3;
		else
			return null;
	}

	@Override
	void removeContact(Contact contact) {
		if (contact == no3) {
			no3 = null;
		} else if (contact == nc3) {
			nc3 = null;
		} else {
			super.removeContact(contact);
		}
	}

}
