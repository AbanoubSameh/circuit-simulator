/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

import java.util.LinkedList;

public class Node extends Component {

	private LinkedList<Component> components = new LinkedList<Component>();

	@Override
	void setNext(Component component) {

		if (component.getClass() == terminals.Node.class) {
			System.out.println("something wrong is going on up here");
		}

		components.add(component);
	}

	@Override
	void setPrevious(Component component) {

		if (component.getClass() == terminals.Node.class) {
			System.out.println("something wrong is going on down here");
		}

		components.add(component);
	}

	void removeComponent(Component component) {

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i) == component) {
				components.remove(i);
				return;
			}

		}

	}

	void removeComponents() {

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i) == null) {
				components.remove(i);
			}

		}

	}

	@Override
	void setVoltage(int voltage, Component component) {

		int highestVoltage = voltage;

		for (int i = 0; i < components.size(); i++) {

			int temp = -1;

			if (components.get(i) != component) {
				temp = components.get(i).getVoltage(this);
			}

//			System.out.println(components.get(i).toString() + " " + temp);

			if (temp > highestVoltage) {
				highestVoltage = temp;
			}

		}

//		System.out.println("highest voltage: " + highestVoltage);

		for (int i = 0; i < components.size(); i++) {

			components.get(i).setVoltage(highestVoltage, this);

		}

	}

	@Override
	int getVoltage(Component component) {

		if (super.pass(component)) {
			return -1;
		}

		int highestVoltage = -1;

		for (int i = 0; i < components.size(); i++) {

			int temp = -1;

			if (components.get(i) != this && !pass(components.get(i))) {
				temp = components.get(i).getVoltage(this);
			}

			if (temp > highestVoltage) {
				highestVoltage = temp;
			}

		}

		return highestVoltage;

	}

	@Override
	public void disconnect() {

		for (int i = 0; i < components.size(); i++) {

			Component temp = components.get(i);

			if (temp.next == this) {
				temp.setVoltage(-1, this);
				temp.next = null;
				removeComponent(temp);
			} else if (temp.previous == this) {
				temp.setVoltage(-1, this);
				temp.previous = null;
				removeComponent(temp);
			}

		}

	}

	public Component getInteractiveComponent() {

		for (Component component : components) {
			if (component.previous == this && (component.getClass() == ButtonNO.class
					|| component.getClass() == ButtonNC.class || component.getClass() == Relay.class)) {
				return component;
			}
		}

		return null;

	}

	public Component getNormalComponent() {

		for (Component component : components) {
			if (component.previous == this && component.getClass() != terminals.Wire.class) {
				return component;
			}
		}

		return null;

	}

}
