/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class ContactNC extends Component {

	private boolean closed = true;

	@Override
	public void setVoltage(int voltage, Component component) {

		if (closed) {

			if (component == next && previous != null) {
				previous.setVoltage(voltage, this);
			} else if (component == previous && next != null) {
				next.setVoltage(voltage, this);
			}

		}

	}

	@Override
	public int getVoltage(Component component) {

		if (closed) {

			if (component == next && previous != null) {
				return previous.getVoltage(this);
			} else if (component == previous && next != null) {
				return next.getVoltage(this);
			} else {
				return -1;
			}

		} else {
			return -1;
		}

	}

	public void close() {

		if (closed) {
			return;
		}

		closed = true;

		if (next != null && previous != null) {

			int volt1 = next.getVoltage(this);
			int volt2 = previous.getVoltage(this);

			if (volt1 > volt2) {
				previous.setVoltage(volt1, this);
			} else if (volt2 > volt1) {
				next.setVoltage(volt2, this);
			} else {
				previous.setVoltage(volt2, this);
				next.setVoltage(volt1, this);
			}

		}

	}

	public void open() {

		if (!closed) {
			return;
		}

		closed = false;

		if (next != null && previous != null) {

			next.setVoltage(-1, this);
			previous.setVoltage(-1, this);

		}

	}

}
