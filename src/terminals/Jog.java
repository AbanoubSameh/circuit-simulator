/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

public class Jog extends Component {

	private ButtonNO no;
	private ButtonNC nc;

	public Jog() {
		this.no = new ButtonNO();
		this.nc = new ButtonNC();
	}

	public Component no() {
		return no;
	}

	public Component nc() {
		return nc;
	}

	public void push() {
		no.push();
		nc.push();
	}

	public void release() {
		no.release();
		nc.release();
	}

	public void pulse() {
		no.pulse();
		nc.pulse();
	}

}
