/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package terminals;

import frontend.Simulator;

public class LED extends Component {

	private boolean on = false;

	@Override
	void setVoltage(int voltage, Component component) {

		if (component == next && previous != null) {

			if (Math.abs(previous.getVoltage(this) - voltage) == 220) {
				turnOn();
			} else {
				turnOff();
			}

		} else if (component == previous && next != null) {

			if (Math.abs(next.getVoltage(this) - voltage) == 220) {
				turnOn();
			} else {
				turnOff();
			}

		}

	}

	@Override
	int getVoltage(Component component) {
		return -1;
	}

	private void turnOn() {

		if (!on) {
			on = true;
			Simulator.stateChanged(this, on);
		}

	}

	private void turnOff() {

		if (on) {
			on = false;
			Simulator.stateChanged(this, on);
		}

	}

	public boolean isOn() {
		return on;
	}

}
