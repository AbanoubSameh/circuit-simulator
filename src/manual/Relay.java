/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public class Relay extends Component {

	private boolean energized = false;
	private ContactNO no = new ContactNO(this);
	private ContactNC nc = new ContactNC(this);

	private void energize() {

		energized = true;
		System.out.println("the relay is energized!!!");

		no.close();
		nc.open();

//		Simulator.stateChanged(this, energized);

	}

	private void denergize() {

		energized = false;
		System.out.println("the relay is de-energized!!!");

		no.open();
		nc.close();

//		Simulator.stateChanged(this, energized);

	}

	@Override
	public void setVoltage(int voltage, Component component) {

		if (voltage != this.voltage) {

			this.voltage = voltage;
			this.voltageSetter = component;

			if (this.voltage == 220 && this.ground == 0 && !energized) {
				energize();
			} else if (energized) {
				denergize();
			}

		}

	}

	@Override
	public void setGround(int ground, Component component) {

		if (ground != this.ground) {

			this.ground = ground;

			if (this.voltage == 220 && this.ground == 0 && !energized) {
				energize();
			} else if (energized) {
				denergize();
			}

		}

	}

	public boolean energized() {
		return energized;
	}

	public ContactNO getNO() {
		return no;
	}

	public ContactNC getNC() {
		return nc;
	}

}