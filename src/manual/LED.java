/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public class LED extends Component {

	private boolean on = false;

	@Override
	public void setVoltage(int voltage, Component component) {

		if (this.voltage != voltage) {

			this.voltage = voltage;
			this.voltageSetter = component;

			if (voltage == 220 && ground == 0) {
				on = true;
//				Simulator.stateChanged(this, on);
			} else {
				on = false;
//				Simulator.stateChanged(this, on);
			}

		}

	}

	@Override
	public void setGround(int ground, Component component) {

		if (this.ground != ground) {

			this.ground = ground;
			this.groundSetter = component;

			if (voltage == 220 && ground == 0) {
				on = true;
//				Simulator.stateChanged(this, on);
			} else {
				on = false;
//				Simulator.stateChanged(this, on);
			}

		}

	}

	public boolean isON() {
		return on;
	}

}
