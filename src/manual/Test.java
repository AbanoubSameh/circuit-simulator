/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public class Test {

	public static void main(String[] args) {

		PositiveSupplyLine line = new PositiveSupplyLine(220);
		NegativeSupplyLine line0 = new NegativeSupplyLine(0);

		ButtonNO on = new ButtonNO();
		ButtonNC off = new ButtonNC();

		Node node = new Node();
		Relay rele = new Relay();

		on.setNext(node);
		on.setPrevious(line);

		rele.getNO().setNext(node);
		rele.getNO().setPrevious(line);

		node.setPrevious(on);
		node.setPrevious(rele.getNO());

		node.setNext(rele);
		rele.setPrevious(node);

		line0.setPrevious(rele);
		rele.setNext(line0);

		System.out.println(line.voltage);
		System.out.println(line0.voltage);

		System.out.println("on voltage: " + on.voltage);
		System.out.println("on ground: " + on.ground);
		System.out.println();

		System.out.println("no voltage: " + rele.getNO().voltage);
		System.out.println("no ground: " + rele.getNO().ground);
		System.out.println();

		System.out.println("node voltage: " + node.voltage);
		System.out.println("node ground: " + node.ground);
		System.out.println();

		System.out.println("off voltage: " + off.voltage);
		System.out.println("off ground: " + off.ground);
		System.out.println();

		System.out.println("relay voltage: " + rele.voltage);
		System.out.println("relay ground: " + rele.ground);
		System.out.println();

		System.out.println("relay is energized? " + rele.energized());
		System.out.println();

		on.pulse();

		System.out.println("on voltage: " + on.voltage);
		System.out.println("on ground: " + on.ground);
		System.out.println();

		System.out.println("no voltage: " + rele.getNO().voltage);
		System.out.println("no ground: " + rele.getNO().ground);
		System.out.println();

		System.out.println("node voltage: " + node.voltage);
		System.out.println("node ground: " + node.ground);
		System.out.println();

		System.out.println("off voltage: " + off.voltage);
		System.out.println("off ground: " + off.ground);
		System.out.println();

		System.out.println("relay voltage: " + rele.voltage);
		System.out.println("relay ground: " + rele.ground);
		System.out.println();

		System.out.println("relay is energized? " + rele.energized());

		off.pulse();

		System.out.println("on voltage: " + on.voltage);
		System.out.println("on ground: " + on.ground);
		System.out.println();

		System.out.println("no voltage: " + rele.getNO().voltage);
		System.out.println("no ground: " + rele.getNO().ground);
		System.out.println();

		System.out.println("node voltage: " + node.voltage);
		System.out.println("node ground: " + node.ground);
		System.out.println();

		System.out.println("off voltage: " + off.voltage);
		System.out.println("off ground: " + off.ground);
		System.out.println();

		System.out.println("relay voltage: " + rele.voltage);
		System.out.println("relay ground: " + rele.ground);
		System.out.println();

		System.out.println("relay is energized? " + rele.energized());

	}

}
