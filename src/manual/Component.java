/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public abstract class Component {

	int voltage, ground;
	Component next, previous, voltageSetter, groundSetter;

	Component() {
		next = null;
		previous = null;
		voltageSetter = null;
		groundSetter = null;
		voltage = -1;
		ground = -1;
	}

	public void setNext(Component next) {
		this.next = next;

		System.out.println("set " + next + " as next for " + this);

		if (next.voltage > this.ground) {
			this.setGround(next.getVoltage(), next);
		} else if (next.voltage < this.ground) {
			next.setVoltage(ground, this);
		}

	}

	public void setPrevious(Component previous) {
		this.previous = previous;

		System.out.println("set " + previous + " as previous for " + this);

		if (previous.ground > this.voltage) {
			this.setVoltage(previous.getGround(), previous);
		} else if (previous.ground < this.voltage) {
			previous.setGround(voltage, this);
		}

	}

	public void setVoltage(int voltage, Component component) {

		if (voltage != this.voltage) {

			this.voltage = voltage;
			this.voltageSetter = component;

			if (next == null) {
//			System.out.println("next is null");
				return;
			}

			next.setVoltage(voltage, component);

		}

	}

	public void setGround(int ground, Component component) {

		if (this.ground != ground) {

			this.ground = ground;
			this.groundSetter = component;

			if (previous == null) {
//			System.out.println("previous is null");
				return;
			}

			previous.setGround(ground, component);

		}

	}

	public int getVoltage() {
		return this.voltage;
	}

	public int getGround() {
		return this.ground;
	}

	public Component getNext() {
		return this.next;
	}

	public Component getPrevious() {
		return this.previous;
	}

	public Component getVoltageSetter() {
		return this.voltageSetter;
	}

	public Component getGroundSetter() {
		return this.groundSetter;
	}

}
