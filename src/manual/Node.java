/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

import java.util.LinkedList;

public class Node extends Component {

	private LinkedList<NodeTerminal> components = new LinkedList<NodeTerminal>();

	@Override
	public void setNext(Component component) {
		NodeTerminal node = new NodeTerminal(component, NodeTerminal.Terminal.voltage);
		components.add(node);
	}

	@Override
	public void setPrevious(Component component) {
		NodeTerminal node = new NodeTerminal(component, NodeTerminal.Terminal.ground);
		components.add(node);
	}

	public void removeComponent(Component component) {

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i).component == component) {
				components.remove(i);
				return;
			}

		}

	}

	public void removeComponents() {

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i).component == null) {
				components.remove(i);
			}

		}

	}

	@Override
	public void setVoltage(int voltage, Component component) {

		int highestVoltage = -1;
		Component setter = this;

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i).terminal == NodeTerminal.Terminal.voltage
					&& components.get(i).component.voltageSetter != this
					&& components.get(i).component.getVoltage() > highestVoltage) {
				highestVoltage = components.get(i).component.getVoltage();
				setter = components.get(i).component;

				System.out.println("The component voltage: " + components.get(i).component.getVoltage());

			} else if (components.get(i).terminal == NodeTerminal.Terminal.ground
					&& components.get(i).component.groundSetter != this
					&& components.get(i).component.getGround() > highestVoltage) {
				highestVoltage = components.get(i).component.getGround();
				setter = components.get(i).component;

				System.out.println("The component ground: " + components.get(i).component.getGround());

			}

		}

		if (highestVoltage != this.voltage) {
			this.voltage = highestVoltage;
			this.ground = highestVoltage;
			this.voltageSetter = setter;
			this.groundSetter = setter;
		}

		for (int i = 0; i < components.size(); i++) {

			if (components.get(i).terminal == NodeTerminal.Terminal.voltage) {
				components.get(i).component.setVoltage(this.voltage, this);
			} else if (components.get(i).terminal == NodeTerminal.Terminal.ground) {
				components.get(i).component.setGround(this.voltage, this);
			}

		}

	}

	@Override
	public void setGround(int voltage, Component component) {
		setVoltage(voltage, component);
	}

	public Component getInteractiveComponent() {

		for (NodeTerminal nodeTerminal : components) {
			if (nodeTerminal.terminal == NodeTerminal.Terminal.voltage
					&& (nodeTerminal.component.getClass() == ButtonNO.class
							|| nodeTerminal.component.getClass() == ButtonNC.class
							|| nodeTerminal.component.getClass() == Relay.class)) {
				return nodeTerminal.component;
			}
		}

		return null;

	}

	private static class NodeTerminal {

		private Component component;

		private static enum Terminal {
			voltage, ground
		};

		private Terminal terminal;

		public NodeTerminal(Component component, Terminal type) {
			this.component = component;
			this.terminal = type;
		}

	}

}
