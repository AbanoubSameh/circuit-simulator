/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public class ButtonNC extends Component {

	private boolean pressed = false;

	public void pulse() {

		System.out.println("ButtonNC pulsed");

		pressed = true;

		if (groundSetter == this) {
			ground = -1;

			if (next == null) {
				System.out.println("next is null");
			} else {
				next.setVoltage(ground, this);
			}

		} else if (voltageSetter == this) {
			voltage = -1;

			if (next == null) {
				System.out.println("previous is null");
			} else {
				previous.setGround(voltage, this);
			}

		}

		pressed = false;

		if (voltage > ground) {
			ground = voltage;
			groundSetter = this;

			if (next == null) {
				System.out.println("next is null");
			} else {
				next.setVoltage(ground, this);
			}

		} else if (ground > voltage) {
			voltage = ground;
			voltageSetter = this;

			if (previous == null) {
				System.out.println("previous is null");
			} else {
				previous.setGround(voltage, this);
			}

		}

	}

	public void push() {

		System.out.println("ButtonNC pushed");

		pressed = true;

		if (groundSetter == this) {
			ground = -1;

			if (next == null) {
				System.out.println("next is null");
			} else {
				next.setVoltage(ground, this);
			}

		} else if (voltageSetter == this) {
			voltage = -1;

			if (next == null) {
				System.out.println("previous is null");
			} else {
				previous.setGround(voltage, this);
			}

		}

	}

	public void release() {

		System.out.println("ButtonNC released");

		pressed = false;

		if (voltage > ground) {
			ground = voltage;
			groundSetter = this;

			if (next == null) {
				System.out.println("next is null");
			} else {
				next.setVoltage(ground, this);
			}

		} else if (ground > voltage) {
			voltage = ground;
			voltageSetter = this;

			if (previous == null) {
				System.out.println("previous is null");
			} else {
				previous.setGround(voltage, this);
			}

		}

	}

	@Override
	public void setVoltage(int voltage, Component component) {

		if (voltage != this.voltage) {
			this.voltage = voltage;
			this.voltageSetter = component;
		}

		if (!pressed) {

			this.ground = voltage;
			this.groundSetter = this;

			if (next == null) {
//				System.out.println("next is null");
				return;
			}

			next.setVoltage(ground, this);

		}

	}

	@Override
	public void setGround(int ground, Component component) {

		if (this.ground != ground) {
			this.ground = ground;
			this.groundSetter = component;
		}

		if (!pressed) {

			this.voltage = ground;
			this.voltageSetter = this;

			if (previous == null) {
//				System.out.println("next is null");
				return;
			}

			previous.setGround(voltage, this);

		}

	}

	public boolean isPressed() {
		return pressed;
	}

}
