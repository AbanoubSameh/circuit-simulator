/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

import java.util.LinkedList;

public class NegativeSupplyLine extends Wire {

	final int voltage;

	private LinkedList<Component> components = new LinkedList<Component>();

	@Override
	public void setPrevious(Component component) {
		components.add(component);
	}

	public NegativeSupplyLine(int voltage) {
		this.voltage = voltage;
		this.ground = voltage;
	}

	public void refresh() {

		if (previous == null) {
			System.out.println("nothing is connected");
		} else {
			previous.setGround(voltage, this);
		}

	}

	@Override
	public int getVoltage() {
		return this.voltage;
	}

	@Override
	public int getGround() {
		return this.ground;
	}

	@Override
	public Component getNext() {
		System.out.println("Can't return next");
		return null;
	}

	@Override
	public Component getPrevious() {
		System.out.println("Can't return previous");
		return null;
	}

	@Override
	public Component getVoltageSetter() {
		return this;
	}

	@Override
	public Component getGroundSetter() {
		return this;
	}

}
