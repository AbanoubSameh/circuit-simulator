/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package manual;

public class Wire extends Component {

	@Override
	public void setVoltage(int voltage, Component component) {

		if (this.voltage != voltage) {

			this.voltage = voltage;
			this.ground = voltage;
			this.voltageSetter = component;

			if (next == null) {
//				System.out.println("next is null");
				return;
			}

			next.setVoltage(ground, component);

		}

	}

	@Override
	public void setGround(int ground, Component component) {

		if (this.ground != ground) {

			this.voltage = ground;
			this.ground = ground;
			this.groundSetter = component;

			if (previous == null) {
//				System.out.println("next is null");
				return;
			}

			previous.setGround(ground, component);

		}

	}

}
