/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package frontend;

import terminals.ButtonNC;
import terminals.ButtonNO;
import terminals.Component;
import terminals.LED;
import terminals.NegativeSupplyLine;
import terminals.Node;
import terminals.PositiveSupplyLine;
import terminals.Relay;
import terminals.Relay4Contacts;
import terminals.Relay6Contacts;
import terminals.Wire;

public class Simulator {

	private static Relay6Contacts relay;

	private static final PositiveSupplyLine w220 = new PositiveSupplyLine(220);
	private static final NegativeSupplyLine w0 = new NegativeSupplyLine();

	public static void main(String[] args) {

		@SuppressWarnings("unused")
		Front frontend = new Front();

		for (int i = 60; i < Front.canvasXSize - 20; i += 15) {
			Terminal.addTerminal(w220, i, 60);
			Terminal.addTerminal(i, Front.canvasYSize - 60, w0);
		}

	}

	public static boolean addComponent(String component, int x, int y) {

		Component toAdd = null;

		if (component == "relay") {
			toAdd = new Relay6Contacts();
		} else if (component == "buttonNO") {
			toAdd = new ButtonNO();
		} else if (component == "buttonNC") {
			toAdd = new ButtonNC();
		} else if (component == "contactNO") {
			if (relay != null) {
				toAdd = relay.getNextContactNO();
				if (toAdd == null) {
					return false;
				}
			} else {
				System.out.println("please choose a relay");
				return false;
			}
		} else if (component == "contactNC") {
			if (relay != null) {
				toAdd = relay.getNextContactNC();
				if (toAdd == null) {
					return false;
				}
			} else {
				System.out.println("please choose a relay");
				return false;
			}
		} else if (component == "led") {
			toAdd = new LED();
		} else {
			System.out.println("wrong component");
			return false;
		}

		Terminal.addTerminal(toAdd, x, y + 30);
		Terminal.addTerminal(x, y - 30, toAdd);

		System.out.println(
				"a component has been created at (" + x + "," + (y - 30) + ") and (" + x + "," + (y + 30) + ")");
		return true;

	}

	public static void addNode(int x, int y) {

		Node node = new Node();

		Terminal.addTerminal(x, y, node);

		System.out.println("a new node has been created at (" + x + "," + y + ")");

	}

	public static void addWire(int wireX1, int wireY1, int wireX2, int wireY2) {

		if (wireX1 == wireX2 && wireY1 == wireY2) {
			System.out.println("No wire has been created");
			return;
		}

		Wire toAdd = new Wire();

		Terminal.addTerminal(wireX1, wireY1, toAdd);
		Terminal.addTerminal(toAdd, wireX2, wireY2);

		System.out.println(
				"a wire from (" + wireX1 + "," + wireY1 + ") to (" + wireX2 + "," + wireY2 + ") has been added");

	}

	public static void deleteNode(int x, int y) {
		Node toDisconnect = Terminal.getNode(x, y);
		if (toDisconnect != null) {
			toDisconnect.disconnect();
			System.out.println("A node should have been deleted");
		} else {
			System.out.println("No node deleted");
		}

	}

	public static void deleteWire(Position first, Position second) {
		Wire toDisconnect = Terminal.getWire(first, second);
		if (toDisconnect != null) {
			toDisconnect.disconnect();
			System.out.println("A wire should have been deleted");
		} else {
			System.out.println("No wire deleted");
		}
	}

	public static void deleteComponent(int x, int y) {
		Component toDisconnect = Terminal.getNormalComponent(x, y);
//		System.out.println("(" + x + "," + y + ")");
		if (toDisconnect != null) {
			if (toDisconnect.getClass() == Relay.class || toDisconnect.getClass() == Relay4Contacts.class
					|| toDisconnect.getClass() == Relay6Contacts.class) {
				solveRelay(toDisconnect);
			}
			toDisconnect.disconnect();
			System.out.println("A component should have been deleted");
		} else {
			System.out.println("No component deleted");
		}
	}

	public static void deleteAll() {

		Terminal.deleteAll();
		relay = null;
		for (int i = 60; i < Front.canvasYSize - 20; i += 15) {
			Terminal.addTerminal(w220, i, 60);
			Terminal.addTerminal(i, Front.canvasYSize - 60, w0);
		}
		System.out.println("everything should have been cleared");

	}

	public static void changeState(int x, int y) {

		Component answer = Terminal.getComponent(x, y - 30);

		if (answer == null) {

			System.out.println("No component exists");

		} else {

			if (answer.getClass() == terminals.ButtonNO.class) {

//				if (((ButtonNO) comp).isPressed()) {
//					((ButtonNO) comp).release();
//				} else {
//					((ButtonNO) comp).push();
//				}

				((ButtonNO) answer).pulse();

			} else if (answer.getClass() == terminals.ButtonNC.class) {

//				if (((ButtonNC) comp).isPressed()) {
//					((ButtonNC) comp).release();
//				} else {
//					((ButtonNC) comp).push();
//				}

				((ButtonNC) answer).pulse();

			} else if (answer.getClass() == terminals.Relay.class || answer.getClass() == terminals.Relay4Contacts.class
					|| answer.getClass() == terminals.Relay6Contacts.class) {

				relay = (Relay6Contacts) answer;

			} else {
				System.out.println("This is not a valid component");
			}

		}

	}

	public static void stateChanged(Component component, boolean isOn) {

		Position dim = Terminal.getPosition(component);

		if (dim == null) {
			System.out.println("No such component");
			return;
		}

		int x = dim.x - 30;
		int y = dim.y;

		if (component.getClass() == terminals.Relay.class || component.getClass() == terminals.Relay4Contacts.class
				|| component.getClass() == terminals.Relay6Contacts.class) {
			if (isOn) {
				Front.redraw(x, y, "relayOn");
			} else {
				Front.redraw(x, y, "relayOff");
			}
		} else if (component.getClass() == terminals.LED.class) {
			if (isOn) {
				Front.redraw(x, y, "ledOn");
			} else {
				Front.redraw(x, y, "ledOff");
			}
		} else {
			System.out.println("Couldn't change image");
		}

	}

	private static void solveRelay(Component relayToRemove) {

		if (relay == relayToRemove) {
			relay = null;
		}

		Position position[] = new Position[6];

		if (relayToRemove.getClass() == Relay.class) {
			position[0] = Terminal.getPosition(((Relay) relayToRemove).no());
			position[1] = Terminal.getPosition(((Relay) relayToRemove).nc());
		} else if (relayToRemove.getClass() == Relay4Contacts.class) {
			position[0] = Terminal.getPosition(((Relay4Contacts) relayToRemove).no());
			position[1] = Terminal.getPosition(((Relay4Contacts) relayToRemove).nc());
			position[2] = Terminal.getPosition(((Relay4Contacts) relayToRemove).no2());
			position[3] = Terminal.getPosition(((Relay4Contacts) relayToRemove).nc2());
		} else if (relayToRemove.getClass() == Relay6Contacts.class) {
			position[0] = Terminal.getPosition(((Relay6Contacts) relayToRemove).no());
			position[1] = Terminal.getPosition(((Relay6Contacts) relayToRemove).nc());
			position[2] = Terminal.getPosition(((Relay6Contacts) relayToRemove).no2());
			position[3] = Terminal.getPosition(((Relay6Contacts) relayToRemove).nc2());
			position[4] = Terminal.getPosition(((Relay6Contacts) relayToRemove).no3());
			position[5] = Terminal.getPosition(((Relay6Contacts) relayToRemove).nc3());
		}

		for (int i = 0; i < position.length - 1; i++) {
			if (position[i] != null) {
				Front.removeComponent(position[i].x, position[i].y);
			}
		}

	}

}
