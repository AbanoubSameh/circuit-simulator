/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package frontend;

import java.util.LinkedList;

import terminals.*;

public class ConnectionNode {

	private static LinkedList<ConnectionNode> nodes = new LinkedList<ConnectionNode>();
	private int x, y;

	private Component upper = null;
	private Component lower = null;

	private boolean hasNode = false;

	public static enum Position {
		upper, lower
	};

	public static ConnectionNode getNode(int x, int y) {

		ConnectionNode node = null;

		for (int i = 0; i < nodes.size(); i++) {

			if (nodes.get(i).x == x && nodes.get(i).y == y) {
				node = nodes.get(i);
			}

		}

		return node;
	}

	public static void print() {
		for (ConnectionNode connectionNode : nodes) {
			System.out.println(connectionNode.x + " " + connectionNode.y);
		}
	}

	public ConnectionNode(int x, int y, Component component, Position position) {
		this.x = x;
		this.y = y;

		if (position == Position.upper) {
			this.upper = component;
		} else if (position == Position.lower) {
			this.lower = component;
		} else {
			System.out.println("Somethin went wrong");
		}

		nodes.add(this);

	}

	public ConnectionNode(int x, int y, Node node) {
		this.x = x;
		this.y = y;

		this.upper = node;
		this.lower = node;

		this.hasNode = true;

		nodes.add(this);

	}

	public void setUpper(Component component) {

		if (hasNode) {

			Component.connect(component, upper);

			System.out.println("Found a node here");

			return;
		}

		if (upper == null) {
			this.upper = component;

			Component.connect(upper, lower);

			System.out.println("setting " + upper.toString() + " as previous for " + lower.toString());

		} else {
			System.out.println("The upper of this component is full");
		}

	}

	public void setLower(Component component) {

		if (hasNode) {

			Component.connect(lower, component);

			System.out.println("Found a node here");

			return;
		}

		if (lower == null) {
			this.lower = component;

			Component.connect(upper, lower);

			System.out.println("setting " + lower.toString() + " as next for " + upper.toString());

		} else {
			System.out.println("The lower of this component is full");
		}

	}

	public void setNode(Node node) {

		if (lower != null) {
			Component.connect(upper, lower);
		}

		if (upper != null) {
			Component.connect(upper, lower);
		}

		this.hasNode = true;
		upper = lower = node;

	}

	public Component getUpper() {

		if (upper == null) {
			System.out.println("The upper of this component is null");
		}

		return upper;
	}

	public Component getLower() {

		if (lower == null) {
			System.out.println("The lower of this component is null");
		}

		return lower;
	}

	public boolean hasNode() {
		return hasNode;
	}

	public static void addNode(ConnectionNode node) {

		nodes.add(node);
		System.out.println();

	}

	public static int getNodeX(Component component) {

		for (ConnectionNode connectionNode : nodes) {

			if (connectionNode.lower == component) {
				return connectionNode.x;
			}

		}

		System.out.println("There is no such node");
		return -1;

	}

	public static int getNodeY(Component component) {

		for (ConnectionNode connectionNode : nodes) {

			if (connectionNode.lower == component) {
				return connectionNode.y;
			}

		}

		System.out.println("There is no such node");
		return -1;

	}

	public static boolean hasComponent(Component component) {

		for (ConnectionNode connectionNode : nodes) {

			if (connectionNode.lower == component) {
				return true;
			}

		}

		System.out.println("There is no such component");
		return false;

	}

}
