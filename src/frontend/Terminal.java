/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package frontend;

import java.util.LinkedList;

import terminals.Component;
import terminals.Node;
import terminals.Wire;

public class Terminal {

	private static LinkedList<Terminal> terminals = new LinkedList<Terminal>();

	private int x, y;
	private Component up, down;
	private Node node;
	private boolean hasNode = false;

	private Terminal(int x, int y, Component down) {
		this.x = x;
		this.y = y;
		this.down = down;
		terminals.add(this);
	}

	private Terminal(Component up, int x, int y) {
		this.x = x;
		this.y = y;
		this.up = up;
		terminals.add(this);
	}

	private Terminal(int x, int y, Node node) {
		this.x = x;
		this.y = y;
		this.node = node;
		this.hasNode = true;
		terminals.add(this);
	}

	public void addUp(Component up) {

		if (hasNode) {
			Component.connect(up, node);
			return;
		}

		if (up.getClass() == terminals.Node.class && !hasNode) {
			addNode(((Node) up));
		}

		if (this.up != null) {
			Node toAdd = new Node();
			addNode(toAdd);
			Component.connect(up, node);
			return;
		}

		this.up = up;

		if (down != null) {
			Component.connect(up, down);
		}

	}

	public void addDown(Component down) {

		if (hasNode) {
			Component.connect(node, down);
			return;
		}

		if (down.getClass() == terminals.Node.class && !hasNode) {
			addNode(((Node) down));
		}

		if (this.down != null) {
			Node toAdd = new Node();
			addNode(toAdd);
			Component.connect(node, down);
			return;
		}

		this.down = down;

		if (up != null) {
			Component.connect(up, down);
		}

	}

	public void addNode(Node node) {

		if (hasNode) {
			System.out.println("already has a node");
			return;
		}

		this.node = node;
		hasNode = true;

		if (up != null && up.getClass() != terminals.Node.class) {
			System.out.println(up.toString());
			Component.connect(up, node);
		}

		if (down != null && down.getClass() != terminals.Node.class) {
			System.out.println(down.toString());
			Component.connect(node, down);
		}

	}

	private static Terminal getTerminal(int x, int y) {

		Terminal toFind = null;

		for (Terminal terminal : terminals) {
			if (terminal.x == x && terminal.y == y) {
				toFind = terminal;
			}
		}

		return toFind;

	}

	public static void addTerminal(Component up, int x, int y) {

		Terminal toAdd = getTerminal(x, y);

		if (toAdd == null) {
			toAdd = new Terminal(up, x, y);
		} else {
			toAdd.addUp(up);
		}

	}

	public static void addTerminal(int x, int y, Component down) {

		Terminal toAdd = getTerminal(x, y);

		if (toAdd == null) {
			toAdd = new Terminal(x, y, down);
		} else {
			toAdd.addDown(down);
		}

	}

	public static void addTerminal(int x, int y, Node node) {

		Terminal toAdd = getTerminal(x, y);

		if (toAdd == null) {
			toAdd = new Terminal(x, y, node);
		} else {
			toAdd.addNode(node);
		}

	}

	public static Component getComponent(int x, int y) {

		Terminal temp = getTerminal(x, y);

		if (temp == null || temp.down == null) {
			return null;
		} else {

			if (temp.hasNode) {
				return temp.node.getInteractiveComponent();
			} else {
				return temp.down;
			}

		}

	}

	public static Position getPosition(Component component) {

		Terminal temp = null;

		for (Terminal terminal : terminals) {
			if (terminal.down == component) {
				temp = terminal;
			}
		}

		if (temp != null) {
			return new Position(temp.x, temp.y);
		} else {
			return null;
		}

	}

	public static void deleteAll() {
		terminals = new LinkedList<Terminal>();
	}

	public static Node getNode(int x, int y) {
		Terminal temp = getTerminal(x, y);

		if (temp != null && temp.hasNode) {
			return temp.node;
		} else
			return null;

	}

	public static Wire getWire(Position first, Position second) {
		Terminal temp = getTerminal(first.x, first.y);
		Terminal temp2 = getTerminal(second.x, second.y);
		return Wire.getWire(temp.node, temp2.node);
	}

	public static Component getNormalComponent(int x, int y) {
		Terminal temp = getTerminal(x, y);
//		System.out.println("Terminal: " + x + " " + y);

		if (temp != null) {
			if (temp.hasNode) {
				return temp.node.getNormalComponent();
			} else {
				if (temp.down != null) {
					return temp.down;
				} else
					return null;
			}
		} else
			return null;

	}

}
