/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package frontend;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Front extends JFrame {

	// the window and canvas sizes
	private static final int width = 800;
	private static final int height = 520;
	static final int canvasXSize = 780;
	static final int canvasYSize = 420;

	// screen size
	private static final int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
	private static final int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;

	// these variables control the program
	private static enum ProgramState {
		normal, wire, node, haveFirstPoint, delete
	};

	private static ProgramState programState;
	private static Image toDraw = null;
	private static int wireX, wireY, wireX1, wireY1, wireX2, wireY2;
	private static LineList currentWire;

	// lists of wires, nodes and image shapes
	private static LinkedList<Shape> nodes = new LinkedList<Shape>();
	private static LinkedList<LineList> wires = new LinkedList<LineList>();
	private static LinkedList<ImageShape> images = new LinkedList<ImageShape>();

	// the images used in the program
	private static Image icon, relay, buttonNO, buttonNC, contactNO, contactNC, led;
	private static Image relayOn, ledOn;

	// the canvas
	private static MyCanvas canvas;

	public Front() {

		// setting the title, the close operation, the size, etc
		this.setTitle("Circuit Simulator");
		this.setSize(width, height);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocation(screenWidth / 2 - this.getWidth() / 2, screenHeight / 2 - this.getHeight() / 2);
		this.setResizable(false);

		// creating the canvas
		canvas = new MyCanvas();
		canvas.setBackground(Color.WHITE);
		canvas.setSize(new Dimension(canvasXSize, canvasYSize));

		// loading all the images and setting the icon
		try {

			icon = ImageIO.read(new File("images/icon3.png"));

			relay = ImageIO.read(new File("images/relay3.png"));
			buttonNO = ImageIO.read(new File("images/buttonNO3.png"));
			buttonNC = ImageIO.read(new File("images/buttonNC3.png"));
			contactNO = ImageIO.read(new File("images/contactNO3.png"));
			contactNC = ImageIO.read(new File("images/contactNC3.png"));
			led = ImageIO.read(new File("images/led3.png"));

			relayOn = ImageIO.read(new File("images/relayOn3.png"));
			ledOn = ImageIO.read(new File("images/ledOn3.png"));

		} catch (IOException e) {
			System.out.println("Could not open images");
			return;
		}

		// setting the icon after the image has been loaded successfully
		this.setIconImage(icon);

		// mouse listener
		canvas.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (arg0.getButton() == 1) {

					if (programState == ProgramState.delete) {

						int toDeleteX = (int) (Math.floor((getMousePosition().x - 10) / 15) * 15);
						int toDeleteY = (int) (Math.floor((getMousePosition().y - 35) / 15) * 15);

						for (int i = 0; i < nodes.size(); i++) {
							if (nodes.get(i).contains(toDeleteX, toDeleteY)) {
								nodes.remove(i);
								Simulator.deleteNode(toDeleteX, toDeleteY);
								canvas.repaint();
								return;
							}
						}

						for (int i = 0; i < wires.size(); i++) {
							LineList temp = wires.get(i).intersect(toDeleteX, toDeleteY);
							if (temp != null) {
								Simulator.deleteWire(LineList.getFirstTerminal(temp), LineList.getLastTerminal(temp));
								wires.remove(i);
								canvas.repaint();
								return;
							}
						}

						toDeleteX = (int) (Math.floor((getMousePosition().x - 10) / 60) * 60);
						toDeleteY = (int) (Math.floor((getMousePosition().y - 35) / 60) * 60);
//						System.out.println("Front: " + (toDeleteX + 30)+ " " + toDeleteY);

						for (int i = 0; i < images.size(); i++) {
							if (images.get(i).getX() <= toDeleteX && images.get(i).getX() >= toDeleteX - 60
									&& images.get(i).getY() <= toDeleteY && images.get(i).getY() >= toDeleteY - 60) {
//								System.out.println(images.get(i).getX() + " " + images.get(i).getY());
								images.remove(i);
								Simulator.deleteComponent(toDeleteX + 30, toDeleteY);
								canvas.repaint();
								return;
							}
						}

					} else if (programState == ProgramState.haveFirstPoint) {

						Graphics2D gx = (Graphics2D) canvas.getGraphics();
						gx.setStroke(new BasicStroke(2));

						wireX2 = (int) (Math.floor((getMousePosition().x - 10) / 15) * 15);
						wireY2 = (int) (Math.floor((getMousePosition().y - 35) / 15) * 15);

						// make a new line
						Shape line = new Line2D.Double(wireX, wireY, wireX2, wireY2);
						gx.draw(line);
						currentWire.addLine(line, wireX2, wireY2);

						wireX = (int) (Math.floor((getMousePosition().x - 10) / 15) * 15);
						wireY = (int) (Math.floor((getMousePosition().y - 35) / 15) * 15);

					} else if (programState == ProgramState.wire) {

						wireX = (int) (Math.floor((getMousePosition().x - 10) / 15) * 15);
						wireY = (int) (Math.floor((getMousePosition().y - 35) / 15) * 15);
						wireX1 = wireX;
						wireY1 = wireY;

						if (wires.isEmpty() || !wires.getLast().isEmpty()) {
							currentWire = new LineList(wireX1, wireY1);
							wires.add(currentWire);
						}

						programState = ProgramState.haveFirstPoint;

					} else if (programState == ProgramState.node) {

						int toDrawX = (int) (Math.floor((getMousePosition().x - 10) / 15) * 15 - 4);
						int toDrawY = (int) (Math.floor((getMousePosition().y - 35) / 15) * 15 - 4);

						// make a new circle
						Shape circle = new Ellipse2D.Float(toDrawX, toDrawY, 8, 8);
						((Graphics2D) canvas.getGraphics()).fill(circle);
						nodes.add(circle);

						Simulator.addNode(toDrawX + 4, toDrawY + 4); // call the add node function

					} else if (toDraw != null) {

						int toDrawX = (int) (Math.floor((getMousePosition().x - 10) / 60) * 60);
						int toDrawY = (int) (Math.floor((getMousePosition().y - 35) / 60) * 60);

						// make a new image shape
						ImageShape image = new ImageShape(toDraw, toDrawX, toDrawY);

						// check if another component is present
						for (ImageShape imageShape : images) {
							if (imageShape.getX() == image.getX() && imageShape.getY() == image.getY()) {
								System.out.println("can't draw here");
								return;
							}
						}

						// call the add component function
						if (toDraw == relay) {
							Simulator.addComponent("relay", toDrawX + 30, toDrawY + 30);
						} else if (toDraw == buttonNO) {
							Simulator.addComponent("buttonNO", toDrawX + 30, toDrawY + 30);
						} else if (toDraw == buttonNC) {
							Simulator.addComponent("buttonNC", toDrawX + 30, toDrawY + 30);
						} else if (toDraw == contactNO) {
							if (!Simulator.addComponent("contactNO", toDrawX + 30, toDrawY + 30))
								return;
						} else if (toDraw == contactNC) {
							if (!Simulator.addComponent("contactNC", toDrawX + 30, toDrawY + 30))
								return;
						} else if (toDraw == led) {
							Simulator.addComponent("led", toDrawX + 30, toDrawY + 30);
						} else {
							System.out.println("wrong component");
						}

						canvas.getGraphics().drawImage(image.getImage(), image.getX(), image.getY(), null);
						images.add(image);

					}

				} else if (arg0.getButton() == 3) {

					if (programState == ProgramState.haveFirstPoint) {
						toDraw = null;
						Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
					}

					programState = ProgramState.normal;

					int toChangeX = (int) (Math.floor((getMousePosition().x - 10) / 60) * 60);
					int toChangeY = (int) (Math.floor((getMousePosition().y - 35) / 60) * 60);

					Simulator.changeState(toChangeX + 30, toChangeY + 30); // call the delete function

				}

			}

		});

		// not used now, should be used in the future to resize canvas
		canvas.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) {

				if (arg0.getWheelRotation() == 1) {

				}

			}
		});

		// creating the main panel and the buttons panel
		JPanel panel = new JPanel();
		JPanel buttons = new JPanel();

		// setting the layout for the button panel
		buttons.setLayout(new GridLayout(2, 20));

		// creating the buttons
		JButton relayButton = new JButton("relay");
		JButton nodeButton = new JButton("node");
		JButton buttonNOButton = new JButton("NO button");
		JButton buttonNCButton = new JButton("NC button");
		JButton contactNOButton = new JButton("NO contact");
		JButton contactNCButton = new JButton("NC contact");
		JButton ledButton = new JButton("lamp");
		JButton wireButton = new JButton("wire");
		JButton deleteButton = new JButton("delete");
		JButton deleteAllButton = new JButton("delete all");

		// adding listeners to all of the buttons
		relayButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = relay;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		buttonNOButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = buttonNO;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		buttonNCButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = buttonNC;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		contactNOButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = contactNO;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		contactNCButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = contactNC;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		ledButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toDraw = led;
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.normal;
			}
		});

		nodeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.node;
			}
		});

		wireButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.wire;
			}
		});

		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (programState == ProgramState.haveFirstPoint) {
					Simulator.addWire(wireX1, wireY1, wireX2, wireY2); // call the add wire function
				}
				programState = ProgramState.delete;
			}
		});

		deleteAllButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				programState = null;
				toDraw = null;

				nodes = new LinkedList<Shape>();
				wires = new LinkedList<LineList>();
				images = new LinkedList<ImageShape>();

				canvas.repaint(); // repaint canvas

				Simulator.deleteAll(); // call the delete all function

			}
		});

		// adding all the buttons to the buttons panel
		buttons.add(relayButton);
		buttons.add(buttonNOButton);
		buttons.add(buttonNCButton);
		buttons.add(contactNOButton);
		buttons.add(contactNCButton);
		buttons.add(ledButton);
		buttons.add(nodeButton);
		buttons.add(wireButton);
		buttons.add(deleteButton);
		buttons.add(deleteAllButton);

		// adding the buttons panel and the canvas to the main panel
		panel.add(canvas);
		panel.add(buttons);

		// adding the main panel to the frame
		this.add(panel);

		// setting the frame to visible
		this.setVisible(true);

	}

	public static void removeComponent(int toDeleteX, int toDeleteY) {

		for (int i = 0; i < images.size(); i++) {
			if (images.get(i).getX() <= toDeleteX && images.get(i).getX() >= toDeleteX - 60
					&& images.get(i).getY() <= toDeleteY && images.get(i).getY() >= toDeleteY - 60) {
				images.remove(i);
				Simulator.deleteComponent(toDeleteX, toDeleteY);
				canvas.repaint();
				return;
			}
		}

	}

	// a function used to change the image of a relay or an led
	public static void redraw(int x, int y, String imageName) {

		Image toDraw;

		if (imageName == "relayOn") {
			toDraw = relayOn;
		} else if (imageName == "relayOff") {
			toDraw = relay;
		} else if (imageName == "ledOn") {
			toDraw = ledOn;
		} else if (imageName == "ledOff") {
			toDraw = led;
		} else {
			System.out.println("Could not change image, wrong name");
			return;
		}

		for (ImageShape imageShape : images) {
			if (imageShape.getX() == x && imageShape.getY() == y) {
				imageShape.setImage(toDraw);
			}
		}

		ImageShape image = new ImageShape(toDraw, x, y);
		canvas.getGraphics().drawImage(image.getImage(), image.getX(), image.getY(), null);
		images.add(image);
	}

	// the canvas used in the program
	static class MyCanvas extends Canvas {

		@Override
		public void paint(Graphics g) {

			Graphics2D gr = (Graphics2D) g;
			gr.setStroke(new BasicStroke(1));

			gr.setColor(Color.GREEN);

			for (int i = 1; i < width / 15 + 1; i++) {

				gr.drawLine(15 * i, 0, 15 * i, height);

			}

			for (int i = 1; i < height / 15 + 1; i++) {

				gr.drawLine(0, 15 * i, width, 15 * i);

			}

			gr.setColor(Color.RED);

			for (int i = 1; i < width / 60 + 1; i++) {

				gr.drawLine(60 * i, 0, 60 * i, height);

			}

			for (int i = 1; i < height / 60 + 1; i++) {

				gr.drawLine(0, 60 * i, width, 60 * i);

			}

			gr.setStroke(new BasicStroke(2));
			gr.setColor(Color.ORANGE);
			gr.drawRect(30, 55, 20, 10);
			gr.setColor(Color.BLUE);
			gr.drawLine(20, 60, canvasXSize - 20, 60);
			gr.setColor(Color.BLACK);
			gr.drawLine(20, canvasYSize - 60, canvasXSize - 20, canvasYSize - 60);

		}

		@Override
		public void repaint() {

			Graphics2D gr = (Graphics2D) this.getGraphics();
			gr.setStroke(new BasicStroke(1));

			gr.clearRect(0, 0, canvasXSize, canvasYSize);

			gr.setColor(Color.GREEN);

			for (int i = 1; i < width / 15 + 1; i++) {

				gr.drawLine(15 * i, 0, 15 * i, height);

			}

			for (int i = 1; i < height / 15 + 1; i++) {

				gr.drawLine(0, 15 * i, width, 15 * i);

			}

			gr.setColor(Color.RED);

			for (int i = 1; i < width / 60 + 1; i++) {

				gr.drawLine(60 * i, 0, 60 * i, height);

			}

			for (int i = 1; i < height / 60 + 1; i++) {

				gr.drawLine(0, 60 * i, width, 60 * i);

			}

			gr.setStroke(new BasicStroke(2));
			gr.setColor(Color.ORANGE);
			gr.drawRect(30, 55, 20, 10);
			gr.setColor(Color.BLUE);
			gr.drawLine(20, 60, canvasXSize - 20, 60);
			gr.setColor(Color.BLACK);
			gr.drawLine(20, canvasYSize - 60, canvasXSize - 20, canvasYSize - 60);

			if (!images.isEmpty()) {
				for (ImageShape image : images) {
					((Graphics2D) this.getGraphics()).drawImage(image.getImage(), image.getX(), image.getY(), null);
				}
			}

			if (!nodes.isEmpty()) {
				for (Shape node : nodes) {
					((Graphics2D) this.getGraphics()).fill(node);
				}
			}

			if (!wires.isEmpty()) {
				gr.setStroke(new BasicStroke(2));
				gr.setColor(Color.BLACK);
				for (LineList wireList : wires) {
					for (Shape wire : wireList.getList()) {
						gr.draw(wire);
					}
				}
			}

		}

	}

}
