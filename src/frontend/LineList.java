/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of circuit-simulator.
 *
 * circuit-simulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * circuit-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with circuit-simulator.  If not, see <https://www.gnu.org/licenses/>.
 */

package frontend;

import java.awt.Shape;
import java.util.LinkedList;

public class LineList {

	private LinkedList<Shape> lines = new LinkedList<Shape>();
	private boolean empty = true;

	private int x1, x2, y1, y2;

	public LineList(int x, int y) {
		this.x1 = x;
		this.y1 = y;
	}

	public void addLine(Shape line, int x, int y) {
		empty = false;
		lines.add(line);
		this.x2 = x;
		this.y2 = y;
	}

	public LineList intersect(int x, int y) {

		LineList intersects = null;

		for (Shape line : lines) {
			if (line.intersects(x, y, x, y)) {
				intersects = this;
			}
		}

		return intersects;
	}

	public static Position getFirstTerminal(LineList list) {
		return new Position(list.x1, list.y1);
	}

	public static Position getLastTerminal(LineList list) {
		return new Position(list.x2, list.y2);
	}

	public LinkedList<Shape> getList() {
		return lines;
	}

	public boolean isEmpty() {
		return empty;
	}

}
